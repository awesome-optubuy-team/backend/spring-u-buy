package com.optubuy.springubuy.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class appUtil {
    public static <T> T readTag(Class<T> cls, String input) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(input, cls); // request.getReader()是request body的stream
        } catch (JsonParseException | JsonMappingException e) {
            return null;
        }
    }

    // encrypt password with md5 hex
    public static String encryptPassword(String userId, String password) throws IOException {
        return DigestUtils.md5Hex(userId + DigestUtils.md5Hex(password)).toLowerCase();
    }
}
