package com.optubuy.springubuy.utils;

import com.optubuy.springubuy.model.*;
import com.optubuy.springubuy.service.GameService;
import com.optubuy.springubuy.service.ProductService;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.Mappings;

import java.util.*;

public class ProductMapperImpl {
    /*
    * DTO design pattern using mapstruct
    * ref: https://www.baeldung.com/mapstruct
    * https://github.com/mapstruct/mapstruct/issues/1148
    * */

    GameService gameService;
    ProductService productService;

    public ProductMapperImpl() {
    }

    public ProductMapperImpl(GameService gameService,
                             ProductService productService) {
        this.gameService = gameService;
        this.productService = productService;
    }

    public Product SteamGameToProduct(SteamGame steamGame, boolean saveProduct) {
        if (steamGame == null) return null;

        Product newProduct = new Product(
                ProductCategory.GAME,
                steamGame.getName(),
                steamGame.getGameUrl()
        );
        newProduct.setProductDescription(steamGame.getDescSnippet());
        newProduct.setItem(SteamGameToGame(steamGame, true));

        if (saveProduct) productService.save(newProduct);

        return newProduct;
    }

    protected Game SteamGameToGame(SteamGame steamGame, boolean saveGame) {
        if (steamGame == null) return null;

        String simplifiedTags = mapBroadTags(steamGame.getPopularTags()); // reduce tag types to 10 pre-defned ones
        Game newGame = new Game(
                simplifiedTags,
                steamGame.getReleaseDate(),
                steamGame.getPublisher(),
                null);

        if (saveGame) gameService.save(newGame);

        return newGame;
    }

    public String mapBroadTags(String longTags) {
        // change game.popular_tags from more than 200 tag types to 10 pre-defined types
        String[] tagArray = longTags.split(",");
        Set<String> newTagList = new HashSet<>();
        Map<String, String> map = new HashMap<String, String>(){{
            put("shoot", TagType.Shooter.toString());
            put("fps", TagType.Shooter.toString());
            put("tps", TagType.Shooter.toString());
            put("action", TagType.Action.toString());
            put("adventure", TagType.Adventure.toString());
            put("fight", TagType.Fighting.toString());
            put("rpg", TagType.RPG.toString());
            put("rts", TagType.RTS.toString());
            put("strategy", TagType.RTS.toString());
            put("simu", TagType.Simulation.toString());
            put("sports", TagType.Sports.toString());
            put("battle", TagType.BattleRoyale.toString());
            put("racing", TagType.Racing.toString());
        }};

        for (String tag: tagArray) {
            for (String key: map.keySet()){
                if (tag.toLowerCase().contains(key)){
                    newTagList.add(map.get(key));
                }
            }
        }
        return String.join(",", newTagList);
    }
}


// mapstruct not working
//package com.optubuy.springubuy.utils;
//
//        import com.optubuy.springubuy.model.Game;
//        import com.optubuy.springubuy.model.Product;
//        import com.optubuy.springubuy.model.SteamGame;
//        import org.mapstruct.Mapper;
//        import org.mapstruct.Mapping;
//        import org.mapstruct.Mappings;
//
//@Mapper(componentModel = "spring")
//public interface ProductMapper {
//
//    @Mappings({
//            @Mapping(source = "steamGame.gameUrl", target = "purchaseUrl"),
//            @Mapping(source = "steamGame.name", target = "productName"),
//            @Mapping(source = "steamGame.descSnippet", target = "productDescription"),
//            @Mapping(source = "steamGame.releaseDate", target = "item.releaseDate"),
//            @Mapping(source = "steamGame.publisher", target = "item.company"),
//            @Mapping(source = "steamGame.popularTags", target = "item.popularTags")
//    })
//    Product SteamGameToProduct(SteamGame steamGame);
//}
