package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.ProductCategory;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

@Validated
public interface FavoriteService {
    List<UserProductRelation> getFavorites();
    List<UserProductRelation> getFavoritesPaginated(int page, int postsPerPage);
    List<UserProductRelation> getFavoritesByUserId(long userId);
    UserProductRelation setFavorite(User user, Product product);
    UserProductRelation unsetFavorite(User user, Product product);
//    void delete(UserProductRelation favorite);
}
