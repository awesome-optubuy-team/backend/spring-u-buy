/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.helper.CSVHelper;
import com.optubuy.springubuy.model.SteamGame;
import com.optubuy.springubuy.repository.SteamGameRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class GameCSVServiceImpl implements GameCSVService {

    SteamGameRepository steamGameRepository;

    public GameCSVServiceImpl(SteamGameRepository steamGameRepository) {
        this.steamGameRepository = steamGameRepository;
    }

    @Override
    public void saveGames(MultipartFile file) {
        try {
            List<SteamGame> steamGames = CSVHelper.csv2SteamGames(file.getInputStream());
            steamGameRepository.saveAll(steamGames);
        } catch (IOException e) {
            throw new RuntimeException("Fail to store CSV data:/");
        }
    }

    @Override
    public List<SteamGame> getAllSteamGames() {
        return steamGameRepository.findAll();
    }
}
