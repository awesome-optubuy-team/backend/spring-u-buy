/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.User;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface UserService {
    List<User> getAllUsers();
    List<User> getAllUsersPaginated(int page, int postsPerPage);
    User getUserById(Long id);
    User getUserByUsername(String username);
    User save(User user);
    void delete(User user);
}
