/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.helper.CSVHelper;
import com.optubuy.springubuy.model.SteamGame;
import com.optubuy.springubuy.model.SteamUser;
import com.optubuy.springubuy.repository.SteamUserRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class UserCSVServiceImpl implements UserCSVService {

    SteamUserRepository steamUserRepository;

    public UserCSVServiceImpl(SteamUserRepository steamUserRepository) {
        this.steamUserRepository = steamUserRepository;
    }

    @Override
    public void saveUsers(MultipartFile file) {
        try {
            List<SteamUser> steamUsers = CSVHelper.csv2SteamUsers(file.getInputStream());
            steamUserRepository.saveAll(steamUsers);
        } catch (IOException e) {
            throw new RuntimeException("Fail to store CSV data:/");
        }
    }

    @Override
    public List<SteamUser> getAllSteamUsers() {
        return null;
    }
}
