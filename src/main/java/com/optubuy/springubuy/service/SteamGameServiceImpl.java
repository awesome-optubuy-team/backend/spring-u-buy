/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.SteamGame;
import com.optubuy.springubuy.repository.SteamGameRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SteamGameServiceImpl implements SteamGameService {

    SteamGameRepository steamGameRepository;

    public SteamGameServiceImpl(SteamGameRepository steamGameRepository) {
        this.steamGameRepository = steamGameRepository;
    }

    @Override
    public List<SteamGame> getAllSteamGames() {
        return steamGameRepository.findAll();
    }

    @Override
    public List<SteamGame> getAllSteamGamesPaginated(int page, int steamGamesPerPage) {
        Page<SteamGame> steamGamesOfPage = steamGameRepository.findAll(
                PageRequest.of(page, steamGamesPerPage, Sort.by("releaseDate").descending()));

        return steamGamesOfPage.getContent();
    }
}
