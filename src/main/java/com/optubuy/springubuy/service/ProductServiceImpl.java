/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.exception.ResourceNotFoundException;
import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll(Sort.by(
                Sort.Direction.DESC, "timestamp"));
    }

    @Override
    public List<Product> getAllProductsPaginated(int page, int productsPerPage) {
        Page<Product> productsOfPage = productRepository.findAll(
                PageRequest.of(page, productsPerPage, Sort.by("timestamp").descending()));

        return productsOfPage.getContent();
    }
//
//    @Override
//    public List<Product> getProductsByCategory(List<Product> products, String category) {
//        List<Product> productsOfCategory = new ArrayList<>();
//        return null;
//    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Product not found:(")
        );
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Product product) {
        productRepository.delete(product);
    }
}
