/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllUsersPaginated(int page, int usersPerPage) {
        Page<User> usersOfPage = userRepository.findAll(PageRequest.of(page, usersPerPage));

        return usersOfPage.getContent();
    }

    @Override
    public User getUserById(Long id) {
        Optional<User> res = userRepository.findById(id); // findById returns Optional<User>, .get() will return User or null
        if (res.isPresent()) return res.get();
        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        Optional<User> res = userRepository.findByUsername(username);
        if (res.isPresent()) return res.get();
        return null;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
}
