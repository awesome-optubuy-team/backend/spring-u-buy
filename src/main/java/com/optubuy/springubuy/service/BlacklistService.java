package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface BlacklistService {
    List<UserProductRelation> getBlacklistedByUserId(long userId);
    UserProductRelation setBlacklist(User user, Product product);
    UserProductRelation unsetBlacklist(User user, Product product);
}
