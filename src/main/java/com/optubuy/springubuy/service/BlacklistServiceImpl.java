package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import com.optubuy.springubuy.repository.UserProductRelationRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BlacklistServiceImpl implements BlacklistService {

    UserProductRelationRepository blacklistRepository;

    public BlacklistServiceImpl(UserProductRelationRepository blacklistRepository) {
        this.blacklistRepository = blacklistRepository;
    }

    @Override
    public List<UserProductRelation> getBlacklistedByUserId(long userId) {
        return blacklistRepository.findByBlacklistedAndUser_UserId(true, userId);
    }

    @Transactional
    @Override
    public UserProductRelation setBlacklist(@NonNull User user, @NonNull Product product) {
        UserProductRelation relation = blacklistRepository
                .findByUserAndProduct(user, product);

        // if relation doesn't exist yet, create
        if (relation == null) {
            System.out.println("User-product relation doesn't exist, create new relation.");
            relation = new UserProductRelation(user, product);
            relation.setBlacklisted(true); // set blacklisted as true
            System.out.println("saving new relation to db...");
            blacklistRepository.save(relation);
        }

        // if relation exists, make sure blacklisted is true
        Boolean blacklisted = relation.getBlacklisted();
        if (blacklisted == null || !blacklisted) {
            relation.setBlacklisted(true);
        }

        return relation;
    }

    @Transactional
    @Override
    public UserProductRelation unsetBlacklist(@NonNull User user, @NonNull Product product) {
        UserProductRelation relation = blacklistRepository
                .findByUserAndProduct(user, product);

        // if relation doesn't exist yet, create
        if (relation == null) {
            System.out.println("User-product relation doesn't exist, create new relation.");
            relation = new UserProductRelation(user, product);
            relation.setBlacklisted(false); // set blacklisted as false
            System.out.println("saving new relation to db...");
            blacklistRepository.save(relation);
        }

        // if relation exists, make sure blacklisted is true
        Boolean blacklisted = relation.getBlacklisted();
        if (blacklisted == null || blacklisted) {
            relation.setBlacklisted(false);
        }

        return relation;
    }
}
