package com.optubuy.springubuy.service;

import com.optubuy.springubuy.exception.ResourceNotFoundException;
import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.ProductCategory;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import com.optubuy.springubuy.repository.UserProductRelationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class FavoriteServiceImpl implements FavoriteService {

    UserProductRelationRepository favoriteRepository;

    public FavoriteServiceImpl(UserProductRelationRepository favoriteRepository) {
        this.favoriteRepository = favoriteRepository;
    }

    @Override
    public List<UserProductRelation> getFavorites() {
        // need to add criteria logic to filter on isFavoirte=TRUE
        // ref: https://www.baeldung.com/rest-api-search-language-spring-data-specifications
        return favoriteRepository.findByIsFavorite(true);
    }

    @Override
    public List<UserProductRelation> getFavoritesPaginated(int page, int favoritesPerPage) {
        Page<UserProductRelation> favoritesOfPage = favoriteRepository.findByIsFavorite(
                true,
                PageRequest.of(page, favoritesPerPage));

        return favoritesOfPage.getContent();
    }

    @Override
    public List<UserProductRelation> getFavoritesByUserId(long userId) {
        return favoriteRepository.findByIsFavoriteAndUser_UserId(true, userId);
    }

    @Transactional
    @Override
    public UserProductRelation setFavorite(@NonNull User user, @NonNull Product product) {

        UserProductRelation relation = favoriteRepository
                .findByUserAndProduct(user, product);

        // if relation doesn't exist yet, create
        if (relation == null) {
            System.out.println("User-product relation doesn't exist, create new relation.");
            relation = new UserProductRelation(user, product);
            relation.setFavorite(true); // set favorite as true
            System.out.println("saving new relation to db...");
            favoriteRepository.save(relation);
        }

        // relation exists, just need to setFavorite to true
        Boolean favorited = relation.getFavorite();
        if (favorited == null || !favorited) {
            relation.setFavorite(true);
        }

        return relation;
    }

    @Transactional
    @Override
    public UserProductRelation unsetFavorite(@NonNull User user, @NonNull Product product) {

        UserProductRelation relation = favoriteRepository
                .findByUserAndProduct(user, product);

        // if relation doesn't exist yet, create
        if (relation == null) {
            System.out.println("User-product relation doesn't exist, create new relation.");
            relation = new UserProductRelation(user, product);
            relation.setFavorite(false); // set favorite as false
            System.out.println("saving new relation to db...");
            favoriteRepository.save(relation);
        }

        // relation exists, just need to setFavorite to false
        Boolean favorited = relation.getFavorite();
        if (favorited == null || favorited) {
            relation.setFavorite(false);
        }

        return relation;
    }

//    @Override
//    public void delete(UserProductRelation favorite) {
//        // use flip logic instead
//        favoriteRepository.delete(favorite);
//    }


}
