/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.service;

import com.optubuy.springubuy.model.Product;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface ProductService {
    List<Product> getAllProducts();
    List<Product> getAllProductsPaginated(int page, int postsPerPage);
//    List<Product> getProductsByCategory(List<Product> products, String category);
//    List<Product> getProductsByKeywords(List<Product> products, List<String> keywords);
    Product getProductById(Long id);
    Product save(Product product);
    void delete(Product product);
}
