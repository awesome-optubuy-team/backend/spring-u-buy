/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "steam_game")
public class SteamGame {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String gameUrl; // game url on Steam

    private String packageType; // type of package - app, sub or bundle

    @Column(unique = true)
    private String name; // name of the game

    @Column(columnDefinition = "LONGBLOB")
    private String descSnippet; // short description of the game

    @Column(columnDefinition = "LONGBLOB")
    private String recentReviews; // recent reviews

    @Column(columnDefinition = "LONGBLOB")
    private String allReviews; // all reviews

    private String releaseDate; // release date

    private String developer; // developer of the game

    private String publisher; // publisher or publishers of the game

    @Column(columnDefinition = "LONGBLOB")
    private String popularTags; // tags

    public SteamGame() {}

    public SteamGame(String gameUrl,String packageType, String name, String descSnippet, String recentReviews,
                     String allReviews, String releaseDate, String developer, String publisher, String popularTags) {
        this.gameUrl = gameUrl;
        this.packageType = packageType;
        this.name = name;
        this.descSnippet = descSnippet;
        this.recentReviews = recentReviews;
        this.allReviews = allReviews;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.publisher = publisher;
        this.popularTags = popularTags;
    }

    public Long getId() {
        return id;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescSnippet() {
        return descSnippet;
    }

    public void setDescSnippet(String descSnippet) {
        this.descSnippet = descSnippet;
    }

    public String getRecentReviews() {
        return recentReviews;
    }

    public void setRecentReviews(String recentReviews) {
        this.recentReviews = recentReviews;
    }

    public String getAllReviews() {
        return allReviews;
    }

    public void setAllReviews(String allReviews) {
        this.allReviews = allReviews;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPopularTags() {
        return popularTags;
    }

    public void setPopularTags(String popularTags) {
        this.popularTags = popularTags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SteamGame steamGame = (SteamGame) o;
        return Objects.equals(id, steamGame.id) && Objects.equals(gameUrl, steamGame.gameUrl) && Objects.equals(packageType, steamGame.packageType) && Objects.equals(name, steamGame.name) && Objects.equals(descSnippet, steamGame.descSnippet) && Objects.equals(recentReviews, steamGame.recentReviews) && Objects.equals(allReviews, steamGame.allReviews) && Objects.equals(releaseDate, steamGame.releaseDate) && Objects.equals(developer, steamGame.developer) && Objects.equals(publisher, steamGame.publisher) && Objects.equals(popularTags, steamGame.popularTags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, gameUrl, packageType, name, descSnippet, recentReviews, allReviews, releaseDate, developer, publisher, popularTags);
    }

    @Override
    public String toString() {
        return "SteamGame{" +
                "id=" + id +
                ", gameUrl='" + gameUrl + '\'' +
                ", packageType='" + packageType + '\'' +
                ", name='" + name + '\'' +
                ", descSnippet='" + descSnippet + '\'' +
                ", recentReviews='" + recentReviews + '\'' +
                ", allReviews='" + allReviews + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", developer='" + developer + '\'' +
                ", publisher='" + publisher + '\'' +
                ", popularTags='" + popularTags + '\'' +
                '}';
    }
}
