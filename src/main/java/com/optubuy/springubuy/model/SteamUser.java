/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "steam_user")
public class SteamUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // User ID on Steam.
    private String steamID;

    // Name of the Steam game.
    private String gameName;

    // Behavior (purchase/play).
    private String behavior;

    // Hours if behavior is play, 1.0 if behavior is purchase.
    private double hours;

    public SteamUser() {}

    public SteamUser(String steamID, String gameName, String behavior, double hours) {
        this.steamID = steamID;
        this.gameName = gameName;
        this.behavior = behavior;
        this.hours = hours;
    }

    public Long getId() {
        return id;
    }

    public String getSteamID() {
        return steamID;
    }

    public void setSteamID(String steamID) {
        this.steamID = steamID;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SteamUser steamUser = (SteamUser) o;
        return Double.compare(steamUser.hours, hours) == 0 &&
                Objects.equals(id, steamUser.id) &&
                Objects.equals(steamID, steamUser.steamID) &&
                Objects.equals(gameName, steamUser.gameName) &&
                Objects.equals(behavior, steamUser.behavior);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, steamID, gameName, behavior, hours);
    }

    @Override
    public String toString() {
        return "SteamUser{" +
                "id=" + id +
                ", steamID='" + steamID + '\'' +
                ", gameName='" + gameName + '\'' +
                ", behavior='" + behavior + '\'' +
                ", hours=" + hours +
                '}';
    }
}
