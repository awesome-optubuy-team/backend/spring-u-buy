package com.optubuy.springubuy.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "user_product_relation")
public class UserProductRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userProductId;

    @ManyToOne
    @NotNull(message="User is required.")
    private User user;

    @ManyToOne
    @NotNull(message="Product is required.")
    private Product product;

    private Boolean recommended;

    private String recommendedTimestamp;

    private Boolean isFavorite;

    private String favoritedTimestamp;

    private Boolean clicked;

    private String clickedTimestamp;

    private Boolean blacklisted;

    public UserProductRelation() {
    }

    public UserProductRelation(
            @NotNull(message = "User ID is required.") User user,
            @NotNull(message = "Username is required.") Product product) {
        this.user = user;
        this.product = product;
    }

    public Long getUserProductId() {
        return userProductId;
    }

    public User getUserId() {
        return user;
    }

    public void setUserId(User userId) {
        this.user = userId;
    }

    public Product getProductId() {
        return product;
    }

    public void setProductId(Product productId) {
        this.product = productId;
    }

    public Boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    public String getRecommendedTimestamp() {
        return recommendedTimestamp;
    }

    public void setRecommendedTimestamp(String recommendedTimestamp) {
        this.recommendedTimestamp = recommendedTimestamp;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public String getFavoritedTimestamp() {
        return favoritedTimestamp;
    }

    public void setFavoritedTimestamp(String favoritedTimestamp) {
        this.favoritedTimestamp = favoritedTimestamp;
    }

    public Boolean getClicked() {
        return clicked;
    }

    public void setClicked(Boolean clicked) {
        this.clicked = clicked;
    }

    public String getClickedTimestamp() {
        return clickedTimestamp;
    }

    public void setClickedTimestamp(String clickedTimestamp) {
        this.clickedTimestamp = clickedTimestamp;
    }

    public Boolean getBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(Boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProductRelation that = (UserProductRelation) o;
        return userProductId.equals(that.userProductId) &&
                user.equals(that.user) &&
                product.equals(that.product) &&
                Objects.equals(recommended, that.recommended) &&
                Objects.equals(recommendedTimestamp, that.recommendedTimestamp) &&
                Objects.equals(isFavorite, that.isFavorite) &&
                Objects.equals(favoritedTimestamp, that.favoritedTimestamp) &&
                Objects.equals(clicked, that.clicked) &&
                Objects.equals(clickedTimestamp, that.clickedTimestamp) &&
                Objects.equals(blacklisted, that.blacklisted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userProductId, user, product, recommended, recommendedTimestamp, isFavorite, favoritedTimestamp, clicked, clickedTimestamp, blacklisted);
    }
}
