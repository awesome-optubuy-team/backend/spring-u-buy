/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "game")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long gameId; // unique identifier

//    @ManyToOne
//    private Genre genre;
    private String popularTags;

    private String releaseDate;

    private String averageRating;

    private int popularity = 0;

    private String company;

    private String country;

    private String consolePlatform;

    public Game() {}

    public Game(String popularTags,
//            Genre genre,
                String releaseDate,
                String company,
                String consolePlatform) {
//        this.genre = genre;
        this.popularTags = popularTags;
        this.releaseDate = releaseDate;
        this.company = company;
        this.consolePlatform = consolePlatform;
    }

//    public Genre getGenre() {
//        return genre;
//    }
//
//    public void setGenre(Genre genre) {
//        this.genre = genre;
//    }


    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getPopularTags() {
        return popularTags;
    }

    public void setPopularTags(String popularTags) {
        this.popularTags = popularTags;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getConsolePlatform() {
        return consolePlatform;
    }

    public void setConsolePlatform(String consolePlatform) {
        this.consolePlatform = consolePlatform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return popularity == game.popularity &&
                Objects.equals(gameId, game.gameId) &&
//                Objects.equals(genre, game.genre) &&
                Objects.equals(popularTags, game.popularTags) &&
                Objects.equals(releaseDate, game.releaseDate) &&
                Objects.equals(averageRating, game.averageRating) &&
                Objects.equals(company, game.company) &&
                Objects.equals(country, game.country) &&
                Objects.equals(consolePlatform, game.consolePlatform);
    }

    @Override
    public int hashCode() {
//        return Objects.hash(gameId, genre, releaseDate, averageRating, popularity, company, country, consolePlatform);
        return Objects.hash(gameId, popularTags, releaseDate, averageRating, popularity, company, country, consolePlatform);
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
//                ", genre='" + genre + '\'' +
                ", popularTags=" + popularTags + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", averageRating='" + averageRating + '\'' +
                ", popularity=" + popularity +
                ", company='" + company + '\'' +
                ", country='" + country + '\'' +
                ", consolePlatform='" + consolePlatform + '\'' +
                '}';
    }
}
