/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.Objects;

import static com.optubuy.springubuy.model.ProductCategory.GAME;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long productId;

    private ProductCategory productCategory = GAME; // enum

    @NotNull(message="Product name is required.")
    private String productName;

    @NotNull(message="Purchase URL is required")
    private String purchaseUrl;

    @Column(columnDefinition = "LONGTEXT")
    private String productDescription;

    @Column(columnDefinition = "LONGTEXT")
    private String imageUrls;

    private double price = 0.0;
    
    @OneToOne
    private Game item;

    private String timestamp;
    private ZoneId timezoneId = ZoneId.of("US/Pacific");

    public Product() {}

    public Product(ProductCategory productCategory,
                   @NotNull(message = "Product name is required.") String productName,
                   @NotNull(message = "Purchase URL is required") String purchaseUrl) {
        this.productCategory = productCategory;
        this.productName = productName;
        this.purchaseUrl = purchaseUrl;
        DateTimeFormatter formatter = DateTimeFormatter
                .ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.US)
                .withZone(timezoneId);
        this.timestamp = formatter.format(Instant.now());
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPurchaseUrl() {
        return purchaseUrl;
    }

    public void setPurchaseUrl(String purchaseUrl) {
        this.purchaseUrl = purchaseUrl;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String imageUrls) {
        this.imageUrls = imageUrls;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Game getItem() {
        return item;
    }

    public void setItem(Game game) {
        this.item = game;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTimezoneId() {
        return timezoneId.toString();
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = ZoneId.of(timezoneId);
        // Update timestamp accordingly.
        DateTimeFormatter formatter = DateTimeFormatter
                .ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.US)
                .withZone(this.timezoneId);
        this.timestamp = formatter.format(Instant.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                Objects.equals(productId, product.productId) &&
                Objects.equals(productCategory, product.productCategory) &&
                Objects.equals(productName, product.productName) &&
                Objects.equals(purchaseUrl, product.purchaseUrl) &&
                Objects.equals(productDescription, product.productDescription) &&
                Objects.equals(imageUrls, product.imageUrls) &&
                Objects.equals(item, product.item) &&
                Objects.equals(timestamp, product.timestamp) &&
                Objects.equals(timezoneId, product.timezoneId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productCategory, productName, purchaseUrl, productDescription, imageUrls, price, item, timestamp, timezoneId);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productCategory='" + productCategory + '\'' +
                ", productName='" + productName + '\'' +
                ", purchaseUrl='" + purchaseUrl + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", imageUrls='" + imageUrls + '\'' +
                ", price=" + price +
                ", item=" + item +
                ", timestamp='" + timestamp + '\'' +
                ", timezoneId=" + timezoneId +
                '}';
    }
}
