package com.optubuy.springubuy.model;

public enum TagType {
    Action,
    Adventure,
    BattleRoyale,
    Fighting,
    Racing,
    RPG,
    RTS,
    Shooter,
    Simulation,
    Sports,
    EMPTY("");

    final String tagType;
    TagType(String s) {
        this.tagType = s;
    }

    TagType() {
        this(null);
    }
}
