/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.helper;

import com.optubuy.springubuy.model.SteamGame;
import com.optubuy.springubuy.model.SteamUser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {

    public static String TYPE = "text/csv";
    static String[] GAME_FILE_HEADERS = {"url", "types", "name", "desc_snippet", "recent_reviews", "all_reviews",
            "release_date", "developer", "publisher", "popular_tags"};
    static String[] USER_FILE_HEADERS = {"user_id", "game_name", "behavior", "hours"};

    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<SteamGame> csv2SteamGames(InputStream inputStream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
                     .withIgnoreHeaderCase().withTrim());) {
            List<SteamGame> steamGames = new ArrayList<SteamGame>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord: csvRecords) {
                SteamGame steamGame = new SteamGame(
                        csvRecord.get(GAME_FILE_HEADERS[0]),
                        csvRecord.get(GAME_FILE_HEADERS[1]),
                        csvRecord.get(GAME_FILE_HEADERS[2]),
                        csvRecord.get(GAME_FILE_HEADERS[3]),
                        csvRecord.get(GAME_FILE_HEADERS[4]),
                        csvRecord.get(GAME_FILE_HEADERS[5]),
                        csvRecord.get(GAME_FILE_HEADERS[6]),
                        csvRecord.get(GAME_FILE_HEADERS[7]),
                        csvRecord.get(GAME_FILE_HEADERS[8]),
                        csvRecord.get(GAME_FILE_HEADERS[9])
                );

                steamGames.add(steamGame);
            }

            return steamGames;
        } catch (IOException e) {
            throw new RuntimeException("Fail to parse CSV:/");
        }
     }

    public static List<SteamUser> csv2SteamUsers(InputStream inputStream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
                     .withIgnoreHeaderCase().withTrim());) {
            List<SteamUser> steamUsers = new ArrayList<SteamUser>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord: csvRecords) {
                SteamUser steamUser = new SteamUser(
                        csvRecord.get(USER_FILE_HEADERS[0]),
                        csvRecord.get(USER_FILE_HEADERS[1]),
                        csvRecord.get(USER_FILE_HEADERS[2]),
                        Double.valueOf(csvRecord.get(USER_FILE_HEADERS[3]))
                );

                steamUsers.add(steamUser);
            }

            return steamUsers;
        } catch (IOException e) {
            throw new RuntimeException("Fail to parse CSV:/");
        }
    }
}
