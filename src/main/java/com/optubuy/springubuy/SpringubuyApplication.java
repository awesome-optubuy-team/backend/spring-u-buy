package com.optubuy.springubuy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.optubuy.springubuy.model.*;
import com.optubuy.springubuy.service.*;
import com.optubuy.springubuy.utils.ProductMapperImpl;
//import org.mapstruct.factory.Mappers;
import com.optubuy.springubuy.utils.appUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootApplication
@RestController
public class SpringubuyApplication {

	private static final Logger log = LoggerFactory.getLogger(SpringubuyApplication.class);

	@GetMapping("/")
	public String home() {
		return "Spring is here!";
	}

	@GetMapping("/index")
	public String index() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringubuyApplication.class, args);
	}

	@Bean
	CommandLineRunner initializeDatabase(ProductService productService,
										 GameService gameService,
										 UserService userService,
										 FavoriteService favoriteService,
										 GameCSVService gameCsvService,
										 SteamGameService steamGameService
	) {
		return args -> {

//			// map steam game to product_game
////			ProductMapperImpl mapper = Mappers.getMapper(ProductMapperImpl.class);
//			ProductMapperImpl mapper = new ProductMapperImpl(gameService, productService);
//
//			List<SteamGame> steamGames = steamGameService.getAllSteamGames();
//			for (SteamGame steamGame: steamGames) {
//
//				Product newProduct = mapper.SteamGameToProduct(steamGame, true);
//				System.out.println(newProduct);
////				productService.save(newProduct);
//			}
//
////			// mock game & product data
////			for (int i = 0; i < 2; i++) {
////				Game game = new Game(
//////						genreService.getAllGenres().get(i % 4), // using tags string instead
////						"RPG,FPS,Multi-player",
////						"2020-02-10",
////						"EA",
////						"PC/XBOX");
////				gameService.save(game);
////			}
//
//			List<Game> games = gameService.getAllGames();
//
////			int j = 0;
////			for (Game game : games) {
////				Product product = new Product(
////						ProductCategory.GAME,
////						"Awesome game " + j++,
////						"https://www.amazon.com/");
////				product.setItem(game);
////				product.setImageUrls(
////						"https://image.api.playstation.com/vulcan/img/rnd/202008/1301/raryqz0xAsk3HutslehRAW4A.png" +
////									"?w=1024&thumb=false," + "https://cdn.vox-cdn.com/thumbor/eDDNBIrZd1zqoT42gCb21ky_1qo" +
////								"=/0x0:1700x960/2720x1530/filters:focal(714x344:986x616)/cdn.vox-cdn.com/uploads" +
////								"/chorus_image/image/57514059/mario.0.jpg");
////				productService.save(product);
////			}
//			List<Product> products = productService.getAllProducts();
//
//			// mock user data
//			User user1 = new User(
//					"hanmeimei",
//					"sexyandiknowit",
//					"hanmeimei@sohot.com");
//			User user2 = new User(
//					"lilei",
//					"toobigisproblematic",
//					"lilei@toobig.com");
//
//			// test JSON tag
////			Tag tag = new Tag(ProductCategory.GAME, genreService.getAllGenres());
//			String tagJSONString = new ObjectMapper().writeValueAsString("FPS,Gore,Action");
//			System.out.println(tagJSONString);
//
//			user1.setTags(tagJSONString);
//			user2.setTags(tagJSONString);
//
//			userService.save(user1);
//			userService.save(user2);
//
////			// test unpack user tags into class
////			String user1TagStr = user1.getTags();
////			Tag user1Tag = appUtil.readTag(Tag.class, user1TagStr);
////			for (Genre genreName: user1Tag.getListOfGenreNames()) {
////				System.out.println(genreName);
////			}
//
//			// establish user-product-relation
//			UserProductRelation relation1 = new UserProductRelation(
//					user1,
//					products.get(0));
//
//			UserProductRelation relation2 = new UserProductRelation(
//					user2,
//					products.get(1));
//
//			relation1.setFavorite(true);
//			relation2.setFavorite(false); // favorites shouldn't include relation2
//
//			favoriteService.setFavorite(user1, products.get(0));
//			favoriteService.setFavorite(user2, products.get(1));
//			favoriteService.getFavorites();


//			// refill steam_game.popular_tags to game.popular_tags
//			ProductMapperImpl mapper = new ProductMapperImpl(gameService, productService);
//			List<SteamGame> steamGames = steamGameService.getAllSteamGames();
//			List<Product> products = productService.getAllProducts();
//			Map<String, Product> mapNameTags = new HashMap<>(); // name: product
//			for (Product product: products) {
//				mapNameTags.put(product.getProductName(), product);
//			}
//			for (SteamGame steamGame: steamGames) {
//				String key = steamGame.getName();
//				if (mapNameTags.containsKey(key)) {
//					Product product = mapNameTags.get(key);
//					product.getItem().setPopularTags(steamGame.getPopularTags());
//					gameService.save(product.getItem());
//				} else {
//					mapper.SteamGameToProduct(steamGame, true);
//				}
//			}
//
//			// map game.tags to the 10-category tags
////			ProductMapperImpl mapper = new ProductMapperImpl(gameService, productService);
//			List<Game> games = gameService.getAllGames();
//			List<Game> newGames = new LinkedList<>();
//			for (Game game: games) {
//				String newTags = mapper.mapBroadTags(game.getPopularTags());
//				game.setPopularTags(newTags);
//				newGames.add(game);
//			}
//			System.out.println(gameService.saveAll(newGames));

//			// ingest review ratings to game
//			List<SteamGame> steamGames = steamGameService.getAllSteamGames();
//			List<Product> products = productService.getAllProducts();
//			Map<String, Product> mapNameTags = new HashMap<>(); // name: product
//			for (Product product: products) {
//				mapNameTags.put(product.getProductName(), product);
//			}
//			Pattern p = Pattern.compile("(\\d+(\\.\\d+)?%)"); // regex extract pattern = ##.#%
//			for (SteamGame steamGame: steamGames) {
//				String key = steamGame.getName();
//				if (mapNameTags.containsKey(key)) {
//					Product product = mapNameTags.get(key);
//					Matcher m = p.matcher(steamGame.getAllReviews());
//					try { // fill game rating with % approval from steam_game.all_reviews
//						if (m.find()) {
//							product.getItem().setAverageRating(m.group(0).split("%")[0]);
//
//						} else {
//							product.getItem().setAverageRating("0");
//						}
//						gameService.save(product.getItem());
//					} catch (Exception e) {
//						continue;
//					}
//				}
//			}

			//			// EXAMPLE: parse % number from review text
//			Pattern p = Pattern.compile("(\\d+(\\.\\d+)?%)");
//			Matcher m = p.matcher("Very Positive,(42,550),- 92% of the 42,550 user reviews for this game are positive.");
//			if (m.find()) {
//				System.out.println(m.group(0).split("%")[0]);
//			}

//			// crawl game images from purchase url
//			List<Product> products = productService.getAllProducts();
////			String imgUrl = getImgUrl("https://store.steampowered.com/app/379720/DOOM/");
////			String imgUrl2 = getImgUrl("https://store.steampowered.com/app/578080/PLAYERUNKNOWNS_BATTLEGROUNDS/");
//			for (Product product : products) {
//				String sourceUrl = product.getPurchaseUrl();
//				String imgUrl = getImgUrl(sourceUrl);
//				if (imgUrl != null) {
//					product.setImageUrls(imgUrl);
//					productService.save(product);
//				}
//			}
		};
	}

	public String getImgUrl(String purchaseUrl) throws Exception {
		// crawl game images from purchase url

		Document doc = Jsoup.connect(purchaseUrl).get();
		try {
			Element img = doc.getElementsByClass("game_header_image_full").first();
			String imgUrl = img.absUrl("src");
			if (img != null) {
				System.out.println(imgUrl);
				return imgUrl;
			}
		} catch (Exception e) {
			// some games might have a DOB check page
			try {
				Element img = (Element) doc.getElementsByClass("img_ctn").first().childNode(1);
				String imgUrl = img.absUrl("src");
				if (img != null) {
					System.out.println(imgUrl);
					return imgUrl;
				}
			} catch (Exception f) {}
		}
		return null;
	}
}