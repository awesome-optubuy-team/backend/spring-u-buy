package com.optubuy.springubuy.repository;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserProductRelationRepository extends JpaRepository<UserProductRelation, Long> {

    // favorite queries
    List<UserProductRelation> findByIsFavoriteAndUser_UserId(boolean is_favorite, long userId);
    UserProductRelation findByUserAndProduct(User user, Product product);

    List<UserProductRelation> findByIsFavorite(boolean is_favorite);
    Page<UserProductRelation> findByIsFavorite(boolean is_favorite, Pageable page);

    // blacklist queries
    List<UserProductRelation> findByBlacklistedAndUser_UserId(boolean is_blacklisted, long userId);
}
