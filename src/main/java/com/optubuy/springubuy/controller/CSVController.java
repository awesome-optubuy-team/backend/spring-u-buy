/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.helper.CSVHelper;
import com.optubuy.springubuy.message.ResponseMessage;
import com.optubuy.springubuy.service.GameCSVService;
import com.optubuy.springubuy.service.UserCSVService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/csv")
public class CSVController {

    private GameCSVService gameCsvService;
    private UserCSVService userCsvService;

    public CSVController(GameCSVService gameCsvService, UserCSVService userCsvService) {
        this.gameCsvService = gameCsvService;
        this.userCsvService = userCsvService;
    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadCSVFile(
            @RequestParam("content_type") String content_type,
            @RequestParam("csv") MultipartFile csv) {
        String message = "";

        if (CSVHelper.hasCSVFormat(csv)) {
            try {
                if (content_type.equals("games")) {
                    gameCsvService.saveGames(csv);
                } else if (content_type.equals("users")) {
                    userCsvService.saveUsers(csv);
                }

                message = "File uploaded successfully:)";

                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "File upload failed:/";

                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a CSV file!";

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
