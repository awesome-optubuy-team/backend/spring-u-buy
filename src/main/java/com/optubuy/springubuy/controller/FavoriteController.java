/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.ProductCategory;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import com.optubuy.springubuy.service.FavoriteService;
import com.optubuy.springubuy.service.ProductService;
import com.optubuy.springubuy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/favorites")
public class FavoriteController {

    private UserService userService;
    private ProductService productService;
    private FavoriteService favoriteService;
    private HttpHeaders responseHeaders;

    public FavoriteController(FavoriteService favoriteService,
                              ProductService productService,
                              UserService userService) {
        this.favoriteService = favoriteService;
        this.userService = userService;
        this.productService = productService;
        this.responseHeaders = new HttpHeaders();
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<Map<String, Object>> getFavorites(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "page_size", defaultValue = "10") int page_size) {

        Map<String, Object> response = new LinkedHashMap<>();
        List<UserProductRelation> favoritesOfPage = favoriteService.getFavoritesPaginated(page - 1, page_size);
//        Map<ProductCategory, List<Product>>
         if (page == 1 && favoritesOfPage.size() == 0) {
            responseHeaders.set("status", "There's no fav in the database:(");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        } else if (favoritesOfPage.size() == 0) {
            responseHeaders.set("status", "No more fav to show.");
            response.put("status", "OK");
            response.put("summary", "No more fav on this page.");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }
        responseHeaders.set("status",
                String.format("Favorites of page %d fetched successfully:)", page));
        response.put("status", "OK");
        response.put("page_size", page_size);
        response.put("page", page);
        response.put("favorites", favoritesOfPage);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // favorite product
    @PostMapping(value = "/set_favorite")
    public ResponseEntity<Map<String, Object>> setFavorite(
            @RequestBody Product productToFavorite,
            @RequestParam(value = "userId") long userId) {

        User user = userService.getUserById(userId);

        Map<String, Object> response = new LinkedHashMap<>();
        UserProductRelation favoriteRelation = favoriteService
                .setFavorite(user, productToFavorite);

        if (favoriteRelation == null) {
            responseHeaders.set("status", "Failed setting favorite.");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        responseHeaders.set("status", "User:" + user.getUserId()
                + " liked product:" + productToFavorite.getProductId());
        response.put("status", "OK");
        response.put("favorited_user_product_relation", favoriteRelation);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // unset favorite product
    @PostMapping(value = "/unset_favorite")
    public ResponseEntity<Map<String, Object>> unsetFavorite(
            @RequestBody Product productToUnfavorite,
            @RequestParam(value = "userId") long userId) {

        User user = userService.getUserById(userId);
        Map<String, Object> response = new LinkedHashMap<>();
        UserProductRelation unfavoriteRelation = favoriteService
                .unsetFavorite(user, productToUnfavorite);

        if (unfavoriteRelation == null) {
            responseHeaders.set("status", "Failed removing favorite.");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        responseHeaders.set("status", "User:" + user.getUserId()
                + " unliked product:" + productToUnfavorite.getProductId());
        response.put("status", "OK");
        response.put("favorited_user_product_relation", unfavoriteRelation);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // get favorites by user id
    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<Map<String, Object>> getFavoritesByUserId(
            @PathVariable(value = "userId") long userId) {

        Map<String, Object> response = new LinkedHashMap<>();
        List<UserProductRelation> favoritesByUser = favoriteService.getFavoritesByUserId(userId);

        if (favoritesByUser == null) {
            responseHeaders.set("status", "Favorites not found :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        } else if (favoritesByUser.size() == 0) {
            responseHeaders.set("status", "User " + userId + " has no favorite, yet ;)");
            response.put("status", "OK");
            response.put("summary", "User " + userId + " has no favorite, yet ;)");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }
        responseHeaders.set("status",
                String.format("Favorites of User %d fetched successfully:)", userId));
        response.put("status", "OK");
        response.put("favorites", favoritesByUser);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }
}
