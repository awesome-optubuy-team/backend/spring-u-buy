package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import com.optubuy.springubuy.service.BlacklistService;
import com.optubuy.springubuy.service.ProductService;
import com.optubuy.springubuy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BlacklistController {

    private UserService userService;
    private ProductService productService;
    private BlacklistService blacklistService;
    private HttpHeaders responseHeaders;

    public BlacklistController(BlacklistService blacklistService,
                              ProductService productService,
                              UserService userService) {
        this.blacklistService = blacklistService;
        this.userService = userService;
        this.productService = productService;
        this.responseHeaders = new HttpHeaders();
    }


    // get blacklisted items by user id
    @GetMapping(value = "blacklist/user/{userId}")
    public ResponseEntity<Map<String, Object>> getBlacklistByUserId(
            @PathVariable(value = "userId") long userId) {

        Map<String, Object> response = new LinkedHashMap<>();
        List<UserProductRelation> blacklistByUser = blacklistService.getBlacklistedByUserId(userId);

        if (blacklistByUser == null) {
            responseHeaders.set("status", "Blacklist not found :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        } else if (blacklistByUser.size() == 0) {
            responseHeaders.set("status", "User " + userId + " has no blacklisted items, yet ;)");
            response.put("status", "OK");
            response.put("summary", "User " + userId + " has no blacklisted items, yet ;)");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }
        responseHeaders.set("status",
                String.format("Blacklist of User: %d fetched successfully:)", userId));
        response.put("status", "OK");
        response.put("blacklist", blacklistByUser);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // blacklist a product
    @PostMapping(value = "/add_blacklist")
    public ResponseEntity<Map<String, Object>> addBlacklist(
            @RequestBody Product productToBlacklist,
            @RequestParam(value = "userId") long userId) {

        User user = userService.getUserById(userId);

        Map<String, Object> response = new LinkedHashMap<>();
        UserProductRelation blacklistRelation = blacklistService
                .setBlacklist(user, productToBlacklist);

        if (blacklistRelation == null) {
            responseHeaders.set("status", "Failed setting blacklist.");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        responseHeaders.set("status", "User:" + user.getUserId()
                + " liked product:" + productToBlacklist.getProductId());
        response.put("status", "OK");
        response.put("blacklisted_user_product_relation", blacklistRelation);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // remove product from blacklist
    @PostMapping(value = "/remove_blacklist")
    public ResponseEntity<Map<String, Object>> removeBlacklist(
            @RequestBody Product productToRemoveFromBlacklist,
            @RequestParam(value = "userId") long userId) {

        User user = userService.getUserById(userId);
        Map<String, Object> response = new LinkedHashMap<>();
        UserProductRelation unblacklistRelation = blacklistService
                .unsetBlacklist(user, productToRemoveFromBlacklist);

        if (unblacklistRelation == null) {
            responseHeaders.set("status", "Failed removing product from blacklist.");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        responseHeaders.set("status", "User:" + user.getUserId()
                + " unliked product:" + productToRemoveFromBlacklist.getProductId());
        response.put("status", "OK");
        response.put("blacklisted_user_product_relation", unblacklistRelation);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

}
