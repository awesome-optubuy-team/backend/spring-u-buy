package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.model.Game;
import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.model.UserProductRelation;
import com.optubuy.springubuy.service.BlacklistService;
import com.optubuy.springubuy.service.FavoriteService;
import com.optubuy.springubuy.service.ProductService;
import com.optubuy.springubuy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class RecommendController {
    private UserService userService;
    private ProductService productService;
    private FavoriteService favoriteService;
    private BlacklistService blacklistService;
    private HttpHeaders responseHeaders;

    public RecommendController(UserService userService,
                               ProductService productService,
                               FavoriteService favoriteService,
                               BlacklistService blacklistService) {
        this.userService = userService;
        this.productService = productService;
        this.favoriteService = favoriteService;
        this.blacklistService = blacklistService;
        this.responseHeaders = new HttpHeaders();
    }

    @GetMapping(value = "/recommend/{userId}")
    public ResponseEntity<Map<String, Object>> recommend(
            @PathVariable(value = "userId") long userId){

        Map<String, Object> response = new LinkedHashMap<>();
        User user = userService.getUserById(userId);
        if (user == null) {
            responseHeaders.set("status", "No such user :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }

        List<Product> products = productService.getAllProducts();
        List<UserProductRelation> blacklisted = blacklistService.getBlacklistedByUserId(userId);
        Set<Product> blacklistedProducts = new HashSet<>();
        for (UserProductRelation relation : blacklisted) {
            blacklistedProducts.add(relation.getProductId());
        }
        List<Product> tagMatchedProducts = new ArrayList<>(); // result holder

        if (user.getTags() != null) { // if user tag already set
            String[] tagList = user.getTags().split(",");
            for (Product product : products) {
                if (blacklistedProducts.contains(product)) continue; // skip blacklisted products
                Game game = product.getItem();
                // push current product.game's tag to hashset for O(1) access
                Set<String> gameTagSet = new HashSet<>();
                StringTokenizer st = new StringTokenizer(game.getPopularTags(), ",");
                while(st.hasMoreTokens()) {
                    gameTagSet.add(st.nextToken());
                }
                // iterate through user's selected tags, add products to result if game tag list contains user tag
                for (String targetTag: tagList) {
                    if (gameTagSet.contains(targetTag)) {
                        tagMatchedProducts.add(product);
                        break;
                    }
                }
            }
        } else { // if user hasn't set tag, user.getTags()==null
            for (Product product : products) {
                if (blacklistedProducts.contains(product)) continue; // skip blacklisted products
                tagMatchedProducts.add(product);
            }
        }

        responseHeaders.set("status",
                String.format("Users:%d recommendation list genereated successfully:)", userId));
        response.put("status", "OK");

        Comparator rankComp = new rankProductByGameRating();
        tagMatchedProducts.sort(rankComp); // rank result by review ratings
        response.put("recommended_products", tagMatchedProducts); // return all tag-matched results. pagination handled by front-end
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    protected class rankProductByGameRating implements Comparator<Product> {
        @Override
        public int compare(Product o1, Product o2) {
            if (o1.getItem().getAverageRating().equals(o2.getItem().getAverageRating())) {
                return 0;
            }
            return Integer.parseInt(o1.getItem().getAverageRating()) >
                    Integer.parseInt(o2.getItem().getAverageRating())
                    ? -1 : 1;
        }
    }
}
