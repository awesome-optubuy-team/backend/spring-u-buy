/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.model.SteamUser;
import com.optubuy.springubuy.service.SteamUserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/steam-users")
public class SteamUserController {
    private SteamUserService steamUserService;
    private HttpHeaders responseHeaders;

    public SteamUserController(SteamUserService steamUserService) {
        this.steamUserService = steamUserService;
        this.responseHeaders = new HttpHeaders();
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<Map<String, Object>> getSteamUsers(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "page_size", defaultValue = "10") int page_size) {
        Map<String, Object> response = new LinkedHashMap<>();
        List<SteamUser> steamUsersOfPage = steamUserService.getAllSteamUsersPaginated(page - 1, page_size);
        if (page == 1 && steamUsersOfPage.size() == 0) {
            responseHeaders.set("status", "There's no steam users in the database:(");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        } else if (steamUsersOfPage.size() == 0) {
            responseHeaders.set("status", "No more steam users to show.");
            response.put("status", "OK");
            response.put("summary", "No more steam users on this page");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }

        responseHeaders.set("status", String.format("Steam games of page %d fetched successfully:)", page));
        response.put("status", "OK");
        response.put("page_size", page_size);
        response.put("page", page);
        response.put("steam_users", steamUsersOfPage);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }
}
