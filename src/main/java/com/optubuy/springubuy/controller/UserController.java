/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.controller;
import com.optubuy.springubuy.model.Product;
import com.optubuy.springubuy.model.User;
import com.optubuy.springubuy.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("/users")
public class UserController {
    private UserService userService;
    private HttpHeaders responseHeaders;

    public UserController(UserService userService) {
        this.userService = userService;
        this.responseHeaders = new HttpHeaders();
    }

    // Fetch all users, display with pagination.
    @GetMapping(value = "/users")
    public ResponseEntity<Map<String, Object>> getUsers(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "page_size", defaultValue = "10") int page_size) {
        Map<String, Object> response = new LinkedHashMap<>();
        List<User> usersOfPage = userService.getAllUsersPaginated(page - 1, page_size);

        if (page == 1 && usersOfPage.size() == 0) {
            responseHeaders.set("status", "There's no user in the database:(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);

        } else if (usersOfPage.size() == 0) {
            responseHeaders.set("status", "No more user to show.");
            response.put("status", "OK");
            response.put("summary", "No more users on this page.");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }
        responseHeaders.set("status",
                String.format("Users of page %d fetched successfully:)", page));
        response.put("status", "OK");
        response.put("page_size", page_size);
        response.put("page", page);
        response.put("users", usersOfPage);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // get user by id
    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<Map<String, Object>> getUserById(
            @PathVariable(value = "userId") long userId) {
        Map<String, Object> response = new LinkedHashMap<>();
        User user = userService.getUserById(userId);

        if (user == null) {
            responseHeaders.set("status", "No such user :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        responseHeaders.set("status",
                String.format("Users:%d fetched successfully:)", userId));
        response.put("status", "OK");
        response.put("user", user);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // get user by username
    @GetMapping(value = "/user/username/{username}")
    public ResponseEntity<Map<String, Object>> getUserByUsername(
            @PathVariable(value = "username") String username) {
        Map<String, Object> response = new LinkedHashMap<>();
        User user = userService.getUserByUsername(username);

        if (user == null) {
            responseHeaders.set("status", "No such user :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }

        responseHeaders.set("status",
                String.format("Username: fetched successfully:)", username));
        response.put("status", "OK");
        response.put("user", user);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // set tags
    @PostMapping(value = "/user/{userId}")
    public ResponseEntity<Map<String, Object>> setUserTags(
            @PathVariable(value = "userId") long userId,
            @RequestBody List<String> tagList) {
        Map<String, Object> response = new LinkedHashMap<>();
        User user = userService.getUserById(userId);

        if (user == null) {
            responseHeaders.set("status", "No such user :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }

        Collections.sort(tagList);
        String tagStr = String.join(",", tagList);
        if (user.getTags() == null || !user.getTags().equals(tagStr)) {
            user.setTags(tagStr);
            userService.save(user); // this would update user when there's change

            responseHeaders.set("status",
                    String.format("User:%d tags updated successfully:)", userId));
            response.put("status", "OK");
            response.put("user", user);
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }

        responseHeaders.set("status",
                String.format("User:%d tags already exists, no DB updates.", userId));
        response.put("status", "OK");
        response.put("user", user);
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // register new user
    @PostMapping(value = "/register")
    public ResponseEntity<Map<String, Object>> register(
            @RequestBody User user) {
        Map<String, Object> response = new LinkedHashMap<>();

        if (user == null) {
            responseHeaders.set("status", "User attributes are invalid.");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }

        User searchUser = userService.getUserByUsername(user.getUsername());
        if ( searchUser != null) {
            responseHeaders.set("status",
                    String.format("Username:%s already exists.", user.getUsername()));
            response.put("status", "409 Conflict. Cannot create user that already exists.");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }

        userService.save(user);
        responseHeaders.set("status",
                String.format("Username:%s created successfully.", user.getUsername()));
        response.put("status", "OK");
        response.put("user", user);
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // login user
    @PostMapping(value = "/login")
    public ResponseEntity<Map<String, Object>> login(
            @RequestParam String username,
            @RequestParam String password) {

        Map<String, Object> response = new LinkedHashMap<>();
        User user = userService.getUserByUsername(username);
        if (user == null) {
            responseHeaders.set("status", "No such user :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }
        if (!user.getPassword().equals(password)) {
            responseHeaders.set("status", "Wrong password :(");
            response.put("status", "404 NOT FOUND");
            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        }

        responseHeaders.set("status",
                String.format("Username:%s logged in successfully.", user.getUsername()));
        response.put("status", "OK");
        response.put("user", user);
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    // logout user
    @GetMapping(value = "/logout")
    public ResponseEntity<Map<String, Object>> logout() {
        Map<String, Object> response = new LinkedHashMap<>();

        responseHeaders.set("status", "User logged out successfully.");
        response.put("status", "OK");
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

}
