/*
 * Copyright (c) 2021 Xin Li.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.optubuy.springubuy.controller;

import com.optubuy.springubuy.model.SteamGame;
import com.optubuy.springubuy.service.SteamGameService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/steam-games")
public class SteamGameController {
    private SteamGameService steamGameService;
    private HttpHeaders responseHeaders;

    public SteamGameController(SteamGameService steamGameService) {
        this.steamGameService = steamGameService;
        this.responseHeaders = new HttpHeaders();
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<Map<String, Object>> getSteamGames(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "page_size", defaultValue = "10") int page_size) {
        Map<String, Object> response = new LinkedHashMap<>();
        List<SteamGame> steamGamesOfPage = steamGameService.getAllSteamGamesPaginated(page - 1, page_size);
        if (page == 1 && steamGamesOfPage.size() == 0) {
            responseHeaders.set("status", "There's no steam games in the database:(");
            response.put("status", "404 NOT FOUND");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.NOT_FOUND);
        } else if (steamGamesOfPage.size() == 0) {
            responseHeaders.set("status", "No more steam games to show.");
            response.put("status", "OK");
            response.put("summary", "No more steam games on this page.");

            return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
        }

        responseHeaders.set("status",
                String.format("Steam games of page %d fetched successfully:)", page));
        response.put("status", "OK");
        response.put("page_size", page_size);
        response.put("page", page);
        response.put("steam_games", steamGamesOfPage);

        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }
}
