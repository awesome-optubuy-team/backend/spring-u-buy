## Backend API Endpoints

### User Service


- **Get all users**
    - URL Patterns: `/users`
    - Method Type: GET
    - Request Parameters: none
    - Response: all users' profiles stored in the backend database, authorized user's access only.


- **Get user by user id**
    - URL Patterns: `/user/{userId}`
    - Method Type: GET
    - Request Parameters: long userId
    - Response: user profile with specified userId, authorized user's access only.

- **Get user by username**
    - URL Patterns: `/user/username/{username}`
    - Method Type: GET
    - Request Parameters: String username
    - Response: user profile with specified username


- **Set user tags**
    - URL Patterns: `/user/{userId}`
    - Method Type: POST
    - Request Parameters: 
        1. long userId
        2. List<String> tagStr in request body
    - Response: save logged-in user's user.tag field with list of tags, authorize through id_token


- **User registration**
    - URL Patterns: `/register`
    - Method Type: POST
    - Request Parameters: 
        - RequestBody with User object to be created. Set UserId to null as backend will auto-generate id upon creation. 
        - Example RequestBody: 
        ```json
        {
            "userId": null,
            "username": "lisi",
            "password": "123abc",
            "email": "lisi@gmail.com",
            "photoUrl": "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
            "tags": "RPG"
        }
        ```
    - Response: UserProductRelation object linking userId and provided product, with "blacklisted" field set to true
    - Response format: 
    ```json
        {
            "userId": 6611,
            "username": "lisi",
            "password": "123abc",
            "email": "lisi@gmail.com",
            "photoUrl": "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
            "tags": "RPG"
        }
    ```

- **User login**
    - URL Patterns: `/login`
    - Method Type: POST
    - Request Parameters: 
        - String username
        - String password 
    - Response: User object or 404 if not found
    - Response format: 
    ```json
        {
            "status": "OK",
            "user": {
                "userId": 6609,
                "username": "zhangsan",
                "password": "123abc",
                "email": "zhangsan@gmail.com",
                "photoUrl": "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
                "tags": "Shooter,RTS"
            }
        }
    ```

- **User logout**
    - URL Patterns: `/logout`
    - Method Type: GET
    - Request Parameters: none
    - Response: Ok
    - Response format: 
    ```json
        {
            "status": "OK"
        }
    ```



### Product Service


- **Get all products by page**

    - URL Patterns: `/products`
    - Method Type: GET
    - Request Parameters: page, page_size
    - Response: all products in the database paginated, accessible for all registered users. 
    - Response format: "products": list of Product objects
    ```json
    {
        "status": "OK",
        "page_size": 10,
        "page": 1,
        "products": [
            {
                "productId": 425,
                "productCategory": "GAME",
                "productName": "Heroes & Generals",
                "purchaseUrl": "https://store.steampowered.com/app/227940/Heroes__Generals/",
                "productDescription": "Shoot, blow sh!t up, fly or bark orders in the ultimate Free-to-Play, large scale, multiplayer, shooter experience. Thousands of players in multiple battles fighting one massive war of nations. The first to capture 15 cities takes the glory.",
                "imageUrls": null,
                "price": 0.0,
                "item": {
                    "gameId": 424,
                    "popularTags": "RTS,Shooter,Action,Adventure,Simulation",
                    "releaseDate": "Oct 18, 2016",
                    "averageRating": null,
                    "popularity": 0,
                    "company": "RETO MOTO,RETO MOTO",
                    "country": null,
                    "consolePlatform": null
                },
                "timestamp": "2/28/21 6:57 PM",
                "timezoneId": "US/Pacific"
            },
            {
                "productId": 421,
                "productCategory": "GAME",
                "productName": "Golf It!",
                "purchaseUrl": "https://store.steampowered.com/app/571740/Golf_It/",
                "productDescription": "Golf It! is a multiplayer Minigolf game with focus on a dynamic, fun and creative multiplayer experience. One of the most exciting features is a Multiplayer Editor, where you can build and play custom maps together with your friends.",
                "imageUrls": null,
                "price": 0.0,
                "item": {
                    "gameId": 420,
                    "popularTags": "RTS,Simulation,Sports",
                    "releaseDate": "Feb 17, 2017",
                    "averageRating": null,
                    "popularity": 0,
                    "company": "Perfuse Entertainment,Perfuse Entertainment",
                    "country": null,
                    "consolePlatform": null
                },
                "timestamp": "2/28/21 6:57 PM",
                "timezoneId": "US/Pacific"
            }
        ]
    }
    ```


- **Get all tag types**

    - URL Patterns: `/products/tag_types`
    - Method Type: GET
    - Request Parameters: none
    - Response: list of 11 types currently defined as tags. This result will not change unless new categories are added. 
    - Response format: "tag_types": list of TagType enums
    ```json
    {
        "status": "OK",
        "tag_types": [
            "Action",
            "Adventure",
            "BattleRoyale",
            "Fighting",
            "Racing",
            "RPG",
            "RTS",
            "Shooter",
            "Simulation",
            "Sports",
            "EMPTY"
        ]
    }
    ```



- **[Not Implemented] Get product by product id**

    - URL Patterns: `/products/product/{productId}`
    - Method Type: GET
    - Request Parameters: id_token
    - Response: product with given productId


- **[Not Implemented] Search product by category**

    - `/products/search`
    - Method Type: GET
    - Request Parameters: keywords, category, state
    - Response: products with specified search query, request parameters can be used in arbitrary combination as long as the id_token is provided


- **[Not Implemented] Add products to product cataglog**

    - `/products`
    - Method Type: POST
    - Request Parameters: none
    - Request Body: JSON Object representing Product body
    - Response: saved product object with given id_token and request body



### Favorite Service


- **Get all favorites**

    - URL Patterns: `/favorites`
    - Method Type: GET
    - Request Parameters: none
    - Response: favorite items for user with matching userUID
    - Response format: "favorites": list of UserProductRelation objects with favorite field = true
    ```json
    {
        "status": "OK",
        "page_size": 10,
        "page": 1,
        "favorites": [
            {
                "userProductId": 435,
                "recommended": null,
                "recommendedTimestamp": null,
                "favoritedTimestamp": null,
                "clicked": null,
                "clickedTimestamp": null,
                "blacklisted": null,
                "favorite": true,
                "productId": {
                    "productId": 429,
                    "productCategory": "GAME",
                    "productName": "GORN",
                    "purchaseUrl": "https://store.steampowered.com/app/578620/GORN/",
                    "productDescription": "GORN is a ludicrously violent VR gladiator simulator, made by Free Lives, the developers of Broforce and Genital Jousting.",
                    "imageUrls": null,
                    "price": 0.0,
                    "item": {
                        "gameId": 428,
                        "popularTags": "Fighting,Action",
                        "releaseDate": "Jul 10, 2017",
                        "averageRating": null,
                        "popularity": 0,
                        "company": "Devolver Digital,Devolver Digital",
                        "country": null,
                        "consolePlatform": null
                    },
                    "timestamp": "2/28/21 6:57 PM",
                    "timezoneId": "US/Pacific"
                },
                "userId": {
                    "userId": 433,
                    "username": "lilei",
                    "password": "toobigisproblematic",
                    "email": "lilei@toobig.com",
                    "photoUrl": null,
                    "tags": "BattleRoyale,Racing,Sports"
                }
            }
        ]
    }
    ```


- **Set favorite**

    - URL Patterns: `/favorites/set_favorite?userId=432`
    - Method Type: POST
    - Request Parameters: 
        - long userId, 
        - Product object in JSON format. example: 
        ```json
            {
            "productId": 431,
            "productCategory": "GAME",
            "productName": "DRAGON BALL FighterZ",
            "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
            "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
            "imageUrls": null,
            "price": 0.0,
            "item": {
                "gameId": 430,
                "popularTags": "Fighting,Anime,2D Fighter,Multiplayer,Action,Competitive,Local Multiplayer,Great Soundtrack,Controller,2.5D,Singleplayer,Arcade,Story Rich,Difficult,e-sports,Fast-Paced,Beat 'em up,Dragons,Adventure,Open World",
                "releaseDate": "Jan 26, 2018",
                "averageRating": null,
                "popularity": 0,
                "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:57 PM",
            "timezoneId": "US/Pacific"
        }
        ```

    - Response: saved favorite object with given user and product
    - Response format: "favorited_user_product_relation": UserProductRelation object with "favorite" field set to true
    ```json
            {
                "status": "OK",
                "favorited_user_product_relation": {
                    "userProductId": 434,
                    "recommended": null,
                    "recommendedTimestamp": null,
                    "favoritedTimestamp": null,
                    "clicked": null,
                    "clickedTimestamp": null,
                    "blacklisted": true,
                    "userId": {
                        "userId": 432,
                        "username": "hanmeimei",
                        "password": "sexyandiknowit",
                        "email": "hanmeimei@sohot.com",
                        "photoUrl": null,
                        "tags": "Shooter,RTS"
                    },
                    "favorite": true,
                    "productId": {
                        "productId": 431,
                        "productCategory": "GAME",
                        "productName": "DRAGON BALL FighterZ",
                        "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
                        "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
                        "imageUrls": null,
                        "price": 0.0,
                        "item": {
                            "gameId": 430,
                            "popularTags": "RTS,Fighting,Action,Adventure,Sports",
                            "releaseDate": "Jan 26, 2018",
                            "averageRating": null,
                            "popularity": 0,
                            "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                            "country": null,
                            "consolePlatform": null
                        },
                        "timestamp": "2/28/21 6:57 PM",
                        "timezoneId": "US/Pacific"
                    }
                }
            }
    ```



- **Unset favorite**

    - URL Patterns: `/favorites/unset_favorite?userId=432`
    - Method Type: POST
    - Request Parameters: 
        - long userId, 
        - Product object in JSON format. example: 
        ```json
                {
                "productId": 431,
                "productCategory": "GAME",
                "productName": "DRAGON BALL FighterZ",
                "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
                "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
                "imageUrls": null,
                "price": 0.0,
                "item": {
                    "gameId": 430,
                    "popularTags": "Fighting,Anime,2D Fighter,Multiplayer,Action,Competitive,Local Multiplayer,Great Soundtrack,Controller,2.5D,Singleplayer,Arcade,Story Rich,Difficult,e-sports,Fast-Paced,Beat 'em up,Dragons,Adventure,Open World",
                    "releaseDate": "Jan 26, 2018",
                    "averageRating": null,
                    "popularity": 0,
                    "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                    "country": null,
                    "consolePlatform": null
                },
                "timestamp": "2/28/21 6:57 PM",
                "timezoneId": "US/Pacific"
            }
        ```

    - Response: unlike a product for given user
    - Response format: "favorited_user_product_relation": UserProductRelation object with favorite set to false
    ```json
    {
        "status": "OK",
        "favorited_user_product_relation": {
            "userProductId": 434,
            "recommended": null,
            "recommendedTimestamp": null,
            "favoritedTimestamp": null,
            "clicked": null,
            "clickedTimestamp": null,
            "blacklisted": true,
            "favorite": false,
            "productId": {
                "productId": 431,
                "productCategory": "GAME",
                "productName": "DRAGON BALL FighterZ",
                "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
                "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
                "imageUrls": null,
                "price": 0.0,
                "item": {
                    "gameId": 430,
                    "popularTags": "RTS,Fighting,Action,Adventure,Sports",
                    "releaseDate": "Jan 26, 2018",
                    "averageRating": null,
                    "popularity": 0,
                    "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                    "country": null,
                    "consolePlatform": null
                },
                "timestamp": "2/28/21 6:57 PM",
                "timezoneId": "US/Pacific"
            },
            "userId": {
                "userId": 432,
                "username": "hanmeimei",
                "password": "sexyandiknowit",
                "email": "hanmeimei@sohot.com",
                "photoUrl": null,
                "tags": "Shooter,RTS"
            }
        }
    }
    ```



### Blacklist Service


- **Get blacklist by user id**

    - URL Patterns: `/blacklist/user/{userId}`
    - Method Type: GET
    - Request Parameters: long userId
    - Response: list of UserProductRelation objects with "blacklisted" field = true
    - Response format: 
    ```json
        {
            "status": "OK",
            "blacklist": [
                {
                    "userProductId": 434,
                    "recommended": null,
                    "recommendedTimestamp": null,
                    "favoritedTimestamp": null,
                    "clicked": null,
                    "clickedTimestamp": null,
                    "blacklisted": true,
                    "userId": {
                        "userId": 432,
                        "username": "hanmeimei",
                        "password": "sexyandiknowit",
                        "email": "hanmeimei@sohot.com",
                        "photoUrl": null,
                        "tags": "Shooter,RTS"
                    },
                    "favorite": true,
                    "productId": {
                        "productId": 431,
                        "productCategory": "GAME",
                        "productName": "DRAGON BALL FighterZ",
                        "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
                        "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
                        "imageUrls": null,
                        "price": 0.0,
                        "item": {
                            "gameId": 430,
                            "popularTags": "RTS,Fighting,Action,Adventure,Sports",
                            "releaseDate": "Jan 26, 2018",
                            "averageRating": null,
                            "popularity": 0,
                            "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                            "country": null,
                            "consolePlatform": null
                        },
                        "timestamp": "2/28/21 6:57 PM",
                        "timezoneId": "US/Pacific"
                    }
                }
            ]
        }
    ```



- **Add product to blacklist for given user**

    - URL Patterns: `/blacklist/user/{userId}`
    - Method Type: POST
    - Request Parameters: 
        - long userId
        - RequestBody with product to be added to blacklist
        ```json
            {
            "productId": 431,
            "productCategory": "GAME",
            "productName": "DRAGON BALL FighterZ",
            "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
            "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
            "imageUrls": null,
            "price": 0.0,
            "item": {
                "gameId": 430,
                "popularTags": "Fighting,Anime,2D Fighter,Multiplayer,Action,Competitive,Local Multiplayer,Great Soundtrack,Controller,2.5D,Singleplayer,Arcade,Story Rich,Difficult,e-sports,Fast-Paced,Beat 'em up,Dragons,Adventure,Open World",
                "releaseDate": "Jan 26, 2018",
                "averageRating": null,
                "popularity": 0,
                "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:57 PM",
            "timezoneId": "US/Pacific"
            }
        ```

    - Response: UserProductRelation object linking userId and provided product, with "blacklisted" field set to true
    - Response format: 
    ```json
    {
        "status": "OK",
        "blacklist": [
            {
                "userProductId": 434,
                "recommended": null,
                "recommendedTimestamp": null,
                "favoritedTimestamp": null,
                "clicked": null,
                "clickedTimestamp": null,
                "blacklisted": true,
                "userId": {
                    "userId": 432,
                    "username": "hanmeimei",
                    "password": "sexyandiknowit",
                    "email": "hanmeimei@sohot.com",
                    "photoUrl": null,
                    "tags": "Shooter,RTS"
                },
                "favorite": true,
                "productId": {
                    "productId": 431,
                    "productCategory": "GAME",
                    "productName": "DRAGON BALL FighterZ",
                    "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
                    "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
                    "imageUrls": null,
                    "price": 0.0,
                    "item": {
                        "gameId": 430,
                        "popularTags": "RTS,Fighting,Action,Adventure,Sports",
                        "releaseDate": "Jan 26, 2018",
                        "averageRating": null,
                        "popularity": 0,
                        "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                        "country": null,
                        "consolePlatform": null
                    },
                    "timestamp": "2/28/21 6:57 PM",
                    "timezoneId": "US/Pacific"
                }
            }
        ]
    }
    ```


- **Remove product from blacklist for given user**

    - URL Patterns: `/remove_blacklist/user/{userId}`
    - Method Type: POST
    - Request Parameters: 
        - long userId
        - RequestBody with product to be added to blacklist
        ```json
            {
            "productId": 431,
            "productCategory": "GAME",
            "productName": "DRAGON BALL FighterZ",
            "purchaseUrl": "https://store.steampowered.com/app/678950/DRAGON_BALL_FighterZ/",
            "productDescription": "DRAGON BALL FighterZ is born from what makes the DRAGON BALL series so loved and famous: endless spectacular fights with its all-powerful fighters.",
            "imageUrls": null,
            "price": 0.0,
            "item": {
                "gameId": 430,
                "popularTags": "Fighting,Anime,2D Fighter,Multiplayer,Action,Competitive,Local Multiplayer,Great Soundtrack,Controller,2.5D,Singleplayer,Arcade,Story Rich,Difficult,e-sports,Fast-Paced,Beat 'em up,Dragons,Adventure,Open World",
                "releaseDate": "Jan 26, 2018",
                "averageRating": null,
                "popularity": 0,
                "company": "BANDAI NAMCO Entertainment,BANDAI NAMCO Entertainment",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:57 PM",
            "timezoneId": "US/Pacific"
            }
        ```

    - Response: UserProductRelation object linking userId and provided product, with "blacklisted" field set to false
    - Response format: 
    ```json
        {
            "status": "OK",
            "blacklisted_user_product_relation": {
                "userProductId": 1060,
                "recommended": null,
                "recommendedTimestamp": null,
                "favoritedTimestamp": null,
                "clicked": null,
                "clickedTimestamp": null,
                "blacklisted": false,
                "userId": {
                    "userId": 432,
                    "username": "hanmeimei",
                    "password": "sexyandiknowit",
                    "email": "hanmeimei@sohot.com",
                    "photoUrl": null,
                    "tags": "Shooter,RTS"
                },
                "favorite": null,
                "productId": {
                    "productId": 35,
                    "productCategory": "GAME",
                    "productName": "Awesome game 10",
                    "purchaseUrl": "https://www.amazon.com/",
                    "productDescription": null,
                    "imageUrls": "https://image.api.playstation.com/vulcan/img/rnd/202008/1301/raryqz0xAsk3HutslehRAW4A.png?w=1024&thumb=false,https://cdn.vox-cdn.com/thumbor/eDDNBIrZd1zqoT42gCb21ky_1qo=/0x0:1700x960/2720x1530/filters:focal(714x344:986x616)/cdn.vox-cdn.com/uploads/chorus_image/image/57514059/mario.0.jpg",
                    "price": 0.0,
                    "item": {
                        "gameId": null,
                        "popularTags": null,
                        "releaseDate": "2020-02-10",
                        "averageRating": null,
                        "popularity": 0,
                        "company": "EA",
                        "country": null,
                        "consolePlatform": "PC/XBOX"
                    },
                    "timestamp": "3/1/21 6:30 PM",
                    "timezoneId": "US/Pacific"
                }
            }
        }
    ```


    ### Recommend Service


- **Recommend by user-selected tags**

    - URL Patterns: `/recommend/{userId}`
    - Method Type: GET
    - Request Parameters: long userId
    - Response: list of Product objects having user-selected tags and not in user's blacklist. Sorted in descending order by game's average user rating. 
    - Response format: 
    ```json
        {
    "status": "OK",
    "recommended_products": [
        {
            "productId": 389,
            "productCategory": "GAME",
            "productName": "Keep Talking and Nobody Explodes",
            "purchaseUrl": "https://store.steampowered.com/app/341800/Keep_Talking_and_Nobody_Explodes/",
            "productDescription": "Find yourself trapped alone in a room with a ticking time bomb. Your friends have the manual to defuse it, but they can't see the bomb, so you're going to have to talk it out – fast!",
            "imageUrls": "https://cdn.akamai.steamstatic.com/steam/apps/341800/header.jpg?t=1601911245",
            "price": 0.0,
            "item": {
                "gameId": 388,
                "popularTags": "RTS,Action,Simulation",
                "releaseDate": "Oct 8, 2015",
                "averageRating": "98",
                "popularity": 0,
                "company": "Steel Crate Games,Steel Crate Games",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:55 PM",
            "timezoneId": "US/Pacific"
        },
        {
            "productId": 351,
            "productCategory": "GAME",
            "productName": "Dungeon Munchies",
            "purchaseUrl": "https://store.steampowered.com/app/799640/Dungeon_Munchies/",
            "productDescription": "Hunt down monsters to cook and eat them! You’ve been revived in a massive underground complex and you must leave this bizarre facility.",
            "imageUrls": "https://cdn.akamai.steamstatic.com/steam/apps/799640/header.jpg?t=1608102583",
            "price": 0.0,
            "item": {
                "gameId": 350,
                "popularTags": "Action,Adventure,RPG",
                "releaseDate": "Jun 5, 2019",
                "averageRating": "98",
                "popularity": 0,
                "company": "maJAJa,maJAJa",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:54 PM",
            "timezoneId": "US/Pacific"
        },
        {
            "productId": 287,
            "productCategory": "GAME",
            "productName": "Portal",
            "purchaseUrl": "https://store.steampowered.com/app/400/Portal/",
            "productDescription": "Portal™ is a new single player game from Valve. Set in the mysterious Aperture Science Laboratories, Portal has been called one of the most innovative new games on the horizon and will offer gamers hours of unique gameplay.",
            "imageUrls": "https://cdn.akamai.steamstatic.com/steam/apps/400/header.jpg?t=1608593358",
            "price": 0.0,
            "item": {
                "gameId": 286,
                "popularTags": "Shooter,Action,Adventure",
                "releaseDate": "Oct 10, 2007",
                "averageRating": "97",
                "popularity": 0,
                "company": "Valve,Valve",
                "country": null,
                "consolePlatform": null
            },
            "timestamp": "2/28/21 6:51 PM",
            "timezoneId": "US/Pacific"
        }
    ]
}
    ```



